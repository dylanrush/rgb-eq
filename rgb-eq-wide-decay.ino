// Dylan Rush
// Using Adafruit graphics libraries.  Some code adapted from
// http://nuewire.com/info-archive/msgeq7-by-j-skoba/

#include <avr/pgmspace.h>
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

#define CLK 8  // MUST be on PORTB! (Use pin 11 on Mega)
#define LAT A3
#define OE  9
#define A   A0
#define B   A1
#define C   A2
// Last parameter = 'true' enables double-buffering, for flicker-free,
// buttery smooth animation.  Note that NOTHING WILL SHOW ON THE DISPLAY
// until the first call to swapBuffers().  This is normal.
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false);

// A5 - read spectrometer
#define SPECT_IN A5
// D12 - spectrometer strobe
#define SPECT_STROBE 12
// D13 - spectrometer reset
#define SPECT_RESET  13

#define BRIGHT A4

// The Adafruit display turns off pixels below this value
#define MIN_VAL 66

int spectrum[7];
int bars[7];

void setup() {
  matrix.begin();
  //Serial.begin(9600);
  pinMode(SPECT_IN, INPUT);
  pinMode(BRIGHT, INPUT);
  pinMode(SPECT_STROBE, OUTPUT);
  pinMode(SPECT_RESET, OUTPUT);
  analogReference(DEFAULT);

  digitalWrite(SPECT_RESET, LOW);
  digitalWrite(SPECT_STROBE, HIGH);
  for (int s=0; s<7; s++) {
    bars[s]=0;
  }
}

void loop() {
  getSpectValues();

  int brightness = analogRead(BRIGHT) * (255.0 / 1024.0);
  //int brightness = 255;
  for(int x=0; x<matrix.width(); x++) {
    int pos = x * 7 / (matrix.width());
    int spectval = spectrum[pos];
    int height=getYValue(spectval);
    int bar = getYValue(bars[pos]);
    drawSpec(x, bar, 255, minValue(brightness/2));
    for (int y=matrix.height()-1; y>height; y--) {
      drawSpec(x, y, 255, minValue(brightness));
    }
    for (int y=height; y>=0; y--) {
      if (y != bar)
        drawSpec(x, y, 0, 0);
    }
  }
  decay();
}

int minValue(int value) {
  return value < MIN_VAL ? MIN_VAL : value;
}

void decay() {
  for (int s=0; s<7; s++) {
    bars[s]-=50;
  }
}

void drawSpec(int x, int y, int sat, int value) {
  matrix.drawPixel(x, y, matrix.ColorHSV(getColor(y), sat, value, true));
}

int getYValue(int spectval) {
  return matrix.height() - (spectval * matrix.height()) / 1024;
}

int getColor(int yValue) {
  return 1200-80*yValue;
}

void getSpectValues() {
 digitalWrite(SPECT_RESET, HIGH);
 digitalWrite(SPECT_RESET, LOW);

 for (int i = 0; i < 7; i++)
 {
   digitalWrite(SPECT_STROBE, LOW);
   delayMicroseconds(30);
   spectrum[i] = analogRead(SPECT_IN);
   if (spectrum[i] > bars[i]) {
     bars[i] = spectrum[i];
   }

   digitalWrite(SPECT_STROBE, HIGH);
 }
}

